# Puppet environment as a fact
Facter.add(:environment) do
  setcode do
    Facter::Util::Resolution.exec('puppet agent --configprint environment')
  end
end
